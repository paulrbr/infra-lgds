# Infrastructure management

Infra management is done in two main steps:

- Server provisioning (asking some cloud provider to give us some computing resources)
- Server configuration (mainly SSHing into the servers and configure all the tools we need)

## Resources provisioning management

Computing resources are provisioned with Terraform. Terraform is a tool which interacts with many cloud providers to avoid manual actions to create/remove/ resources (such as storage, servers, …).

We use a [wrapper script](https://github.com/paulRbr/terraform-makefile/) to execute every Terraform command for simplification purposes. Please follow the [installation instructions](https://github.com/paulRbr/terraform-makefile/#installation) from the wrapper's readme if you need to interact with our infrasturcture resources.

Secrets are managed manually either via environment variables or via [`pass`](https://www.passwordstore.org/).

Resources are described in HCL language in `*.tf` files. This repo currently has a unique environment:

- `lgds-test` visible in the `providers/scaleway/lgds-test` directory (on Paul's personnal Scaleway account)
- `lgds-prod` visible in the `providers/scaleway/lgds-prod` directory (on Béatrice's - LGDS worker - Scaleway account)

### Installation

Please [configure](https://github.com/scaleway/scaleway-sdk-go/blob/master/scw/README.md#scaleway-config) your scaleway credentials locally. (in a `~/.config/scw/config.yaml` file)

Then install Terraform thanks to this command:

```
tf-make provider=scaleway env=lgds-test install
```

### Planning changes

You can do some modification in the `*.tf` files to add/remove/modify resources. Once ready you only need to execute the `plan` command of Terraform:

```
tf-make provider=scaleway env=lgds-test plan
```

Here is an example output creating two new resources (a public IP and a VM instance):

```
------------------------------------------------------------------------

An execution plan has been generated and is shown below.
Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  # scaleway_instance_ip.public_ip will be created
  + resource "scaleway_instance_ip" "public_ip" {
      + address         = (known after apply)
      + id              = (known after apply)
      + organization_id = (known after apply)
      + reverse         = (known after apply)
      + zone            = (known after apply)
    }

  # scaleway_instance_server.odoo-test will be created
  + resource "scaleway_instance_server" "odoo-test" {
      + boot_type                        = (known after apply)
      + disable_dynamic_ip               = false
      + disable_public_ip                = false
      + enable_dynamic_ip                = false
      + enable_ipv6                      = false
      + id                               = (known after apply)
      + image                            = "debian-buster"
      + ip_id                            = (known after apply)
      + ipv6_address                     = (known after apply)
      + ipv6_gateway                     = (known after apply)
      + ipv6_prefix_length               = (known after apply)
      + name                             = (known after apply)
      + organization_id                  = (known after apply)
      + placement_group_policy_respected = (known after apply)
      + private_ip                       = (known after apply)
      + public_ip                        = (known after apply)
      + security_group_id                = (known after apply)
      + state                            = "started"
      + type                             = "DEV1-S"
      + zone                             = (known after apply)

      + root_volume {
          + delete_on_termination = (known after apply)
          + size_in_gb            = (known after apply)
          + volume_id             = (known after apply)
        }
    }

Plan: 2 to add, 0 to change, 0 to destroy.

------------------------------------------------------------------------
```

### Applying changes

If happy with your changes you can now apply them with the `apply` command:

```
tf-make provider=scaleway env=lgds-test apply
```

## System configuration management

Servers are then configured with Ansible. Ansible is a tool which can configure a set of servers by executing a list of tasks on them.

We use [a wrapper Makefile](https://github.com/paulRbr/ansible-makefile) to launch every ansible commands. You will need to install it locally to launch the commands below. Please follow [the instructions](https://github.com/paulRbr/ansible-makefile#installation) from the wrapper repo readme.

This repo currently manages a single environment (called an `inventory` in Ansible vocabulary):

- `lgds-test` - A testing environment for the LGDS parisian cooperative supermarket _(scaleway dynamic inventory)_
- `lgds-prod` - A prod environment for the LGDS parisian cooperative supermarket _(scaleway dynamic inventory)_

### First Installation / Updates

    # Install ansible
    pip install -r requirements.txt
    # Install role dependencies # args='--force' # if you need to update
    ansible-make install roles_path=vendor/

### Dry run a configuration change

    ansible-make dry-run env=<inventory_name>

For example to change configuration of the `lgds-test` instances:

    ansible-make dry-run env=lgds-test

:::warning
We use a dynamic inventory to detect our servers automatically via a remote API access (dynamic inventories). Thus you will need to provide a API token as environment variable to be able to launch the command.
:::

Dynamic inventory example:

    SCW_TOKEN="$(pass scaleway/token 2>&1)" ansible-make dry-run env=lgds-test

### Deploy changes

Replace the `dry-run` action to `run` in the "Dry run configuration change" paragraph once you are happy to apply your changes.
