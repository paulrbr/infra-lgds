terraform {
  backend "s3" {
    bucket                      = "lgds-terraform-prod"
    key                         = "scaleway/lgds-prod/terraform.tfstate"
    region                      = "fr-par"
    endpoint                    = "https://s3.fr-par.scw.cloud"
    skip_credentials_validation = true
    skip_region_validation      = true
  }
}
