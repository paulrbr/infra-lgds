############################################################
# List of public SSH keys which have access to the servers #
############################################################

resource "scaleway_account_ssh_key" "paul_polonium" {
  name       = "paul_polonium"
  public_key = file("../../../public_keys/paul.pub")
}

resource "scaleway_account_ssh_key" "vincent_perso" {
  name       = "vincent_perso"
  public_key = file("../../../public_keys/vincent_perso.pub")
}
