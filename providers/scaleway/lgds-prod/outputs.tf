output "database_public_ips" {
  value = scaleway_instance_ip.public_ip.*.address
}
